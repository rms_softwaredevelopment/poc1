﻿/* This is the Payment Controller class, this class handles all the payment domain objects
 * There are implementations for POST,GET,PUT,PATCH
 * The Domain class for this is payment
 * The LegacyHelper is used to update the existing
*/ 
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DataAccessLayer;
using DomainClasses;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;
using LegacyHelperClasses;

namespace PaymentService.Controllers
{
    public class PaymentsController : ApiController
    {
        private PaymentContext db = new PaymentContext();

        // GET: api/Payments
        public IQueryable<Payment> GetPayment()
        {
            return db.Payment;
        }

        // GET: api/Payments/5
        [ResponseType(typeof(Payment))]
        public async Task<IHttpActionResult> GetPayment(int id)
        {
            Payment payment = await db.Payment.FindAsync(id);
            if (payment == null)
            {
                return NotFound();
            }

            return Ok(payment);
        }

        // PUT: api/Payments/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPayment(int id, Payment payment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != payment.PaymentId)
            {
                return BadRequest();
            }

            db.Entry(payment).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaymentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        // This the Post method of the service
        // This method updates the bew DB as well as the existing DB
        // the payload type is payment
        // POST: api/Payments
        [ResponseType(typeof(Payment))]
        public async Task<IHttpActionResult> PostPayment(Payment payment)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            // Create and allocate the variable to update the existing DB
            Payment paymentCopy = new Payment();
            // Copy the values into the variable to update the existing DB
            // after the copy the data can be massaged or modified accordingly
            PropertyInfo[] infos = typeof(Payment).GetProperties();
            foreach(PropertyInfo info in infos)
            {
                info.SetValue(paymentCopy, info.GetValue(payment, null), null);
            }
            db.Payment.Add(payment);
            await db.SaveChangesAsync();
            // Create the helper class to update the existing DB
            PaymentLegacy paymentLegacy = new PaymentLegacy();
            // Send the values to be updated in  the existing DB
            paymentLegacy.ConnectAndUpdate(paymentCopy);

            return CreatedAtRoute("DefaultApi", new { id = payment.PaymentId }, payment);
        }

        // DELETE: api/Payments/5
        [ResponseType(typeof(Payment))]
        public async Task<IHttpActionResult> DeletePayment(int id)
        {
            Payment payment = await db.Payment.FindAsync(id);
            if (payment == null)
            {
                return NotFound();
            }

            db.Payment.Remove(payment);
            await db.SaveChangesAsync();

            return Ok(payment);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PaymentExists(int id)
        {
            return db.Payment.Count(e => e.PaymentId == id) > 0;
        }
    }
}