﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DomainClasses
{
    public class PaymentType
    {
        [Key]
        public int TypeId { get; set; }
        public string TypeDescription { get; set; }
    }
}
