﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DomainClasses
{
    public class Payment
    {
        public int InvoiceNumber { get; set; }
        public int PaymentId { get; set; }
        public int TypeId { get; set; }
        public decimal Amount { get; set; }
        public string PaymentNote { get; set; }
        public string PaymentVoidNote { get; set; }
        public System.DateTime PaymentDate { get; set; }
        public byte IsVoid { get; set; }
        public byte IsClear { get; set; }
        public int TransferInvoiceNumber { get; set; }
        public string ApprovalCode { get; set; }
        public string BatchNumber { get; set; }
        public string ReferenceNumber { get; set; }
        public int LocationId { get; set; }
        public System.DateTime DepositDate { get; set; }
        public int IsRequiresAttention { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateIP { get; set; }
        public int CreateUserId { get; set; }
        public System.DateTime VoidDate { get; set; }
        public string VoidIP { get; set; }
        public int VoidUserId { get; set; }

       // public virtual PaymentType Type { get; set; }

    }

}
