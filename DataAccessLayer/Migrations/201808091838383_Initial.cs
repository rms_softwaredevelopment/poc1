namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        PaymentId = c.Int(nullable: false, identity: true),
                        InvoiceNumber = c.Int(nullable: false),
                        TypeId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, storeType: "money"),
                        PaymentNote = c.String(maxLength: 128),
                        PaymentVoidNote = c.String(maxLength: 128),
                        PaymentDate = c.DateTime(nullable: false, storeType: "date"),
                        IsVoid = c.Byte(nullable: false),
                        IsClear = c.Byte(nullable: false),
                        TransferInvoiceNumber = c.Int(nullable: false),
                        ApprovalCode = c.String(maxLength: 128),
                        BatchNumber = c.String(maxLength: 128),
                        ReferenceNumber = c.String(maxLength: 128),
                        LocationId = c.Int(nullable: false),
                        DepositDate = c.DateTime(nullable: false, storeType: "date"),
                        IsRequiresAttention = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false, storeType: "date"),
                        CreateIP = c.String(nullable: false, maxLength: 15),
                        CreateUserId = c.Int(nullable: false),
                        VoidDate = c.DateTime(nullable: false, storeType: "date"),
                        VoidIP = c.String(),
                        VoidUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PaymentId);
            
            CreateTable(
                "dbo.PaymentTypes",
                c => new
                    {
                        TypeId = c.Int(nullable: false, identity: true),
                        TypeDescription = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.TypeId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PaymentTypes");
            DropTable("dbo.Payments");
        }
    }
}
