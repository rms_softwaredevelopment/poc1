namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using DomainClasses;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<DataAccessLayer.PaymentContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataAccessLayer.PaymentContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            var payment = new List<Payment>()
            {

                new Payment() { InvoiceNumber= 123, PaymentId = 1, TypeId = 1, Amount = 10, PaymentDate = DateTime.Today, IsClear = 0, IsVoid = 0,TransferInvoiceNumber = 111, LocationId = 1, DepositDate = DateTime.Today, IsRequiresAttention = 1, CreateDate = DateTime.Today, CreateIP = "192.168.1.10", CreateUserId = 5, VoidDate = DateTime.Today, VoidUserId = 2 },
               // new Payment() { InvoiceNumber= 124, PaymentId = 2, TypeId = 2, Amount = 10, PaymentDate = DateTime.Today, CreateDate = DateTime.Today, CreateIP = "192.168.1.10"},
                new Payment() { InvoiceNumber= 124, PaymentId = 2, TypeId = 2, Amount = 10, PaymentDate = DateTime.Today, IsClear = 0, IsVoid = 0,TransferInvoiceNumber = 111, LocationId = 1, DepositDate = DateTime.Today, IsRequiresAttention = 1, CreateDate = DateTime.Today, CreateIP = "192.168.1.10", CreateUserId = 5, VoidDate = DateTime.Today, VoidUserId = 2 },
            };
            context.SaveChanges();
            base.Seed(context);
        }
    }
}
