﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainClasses;

// Data Source=GpilaiLaptop;Integrated Security=True

namespace DataAccessLayer
{
    public class PaymentContext : DbContext
    {
        public PaymentContext() : base   ("ERPPOCDB") { Database.SetInitializer<PaymentContext>(new CreateDatabaseIfNotExists<PaymentContext>()); }
     
        public DbSet<Payment> Payment { get; set; }
        public DbSet<PaymentType> PaymentType { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Payment>()
                 .Property(i => i.Amount)
                 .HasColumnType("Money")
                 .IsRequired();

            modelBuilder.Entity<Payment>()
                 .Property(i => i.PaymentNote)
                 .HasMaxLength(128);

            modelBuilder.Entity<Payment>()
                .Property(i => i.PaymentVoidNote)
                .HasMaxLength(128);

            modelBuilder.Entity<Payment>()
                 .Property(i => i.PaymentDate)
                 .HasColumnType("Date")
                 .IsRequired();

            modelBuilder.Entity<Payment>()
                 .Property(i => i.IsVoid)
                 .HasColumnType("tinyint");

            modelBuilder.Entity<Payment>()
                 .Property(i => i.IsClear)
                 .HasColumnType("tinyint");

            modelBuilder.Entity<Payment>()
               .Property(i => i.ApprovalCode)
               .HasMaxLength(128);

            modelBuilder.Entity<Payment>()
                .Property(i => i.BatchNumber)
                .HasMaxLength(128);

            modelBuilder.Entity<Payment>()
               .Property(i => i.ReferenceNumber)
               .HasMaxLength(128);

            modelBuilder.Entity<Payment>()
                 .Property(i => i.DepositDate)
                 .HasColumnType("Date");

            modelBuilder.Entity<Payment>()
                 .Property(i => i.CreateDate)
                 .HasColumnType("Date")
                 .IsRequired();

            modelBuilder.Entity<Payment>()
                 .Property(i => i.CreateIP)
                 .HasMaxLength(15)
                 .IsRequired();

            modelBuilder.Entity<Payment>()
                .Property(i => i.VoidDate)
                .HasColumnType("Date");

            modelBuilder.Entity<PaymentType>()
               .Property(i => i.TypeDescription)
               .HasMaxLength(128);


        }
    }
}
