﻿/* This is the Payment Legacy Class, this class handles all the payment to the existing Database
 * The implementations for this should reflect the existing Database
 * All the data massaging or mods to tweak to fit the existing database should done within this class
 * The Domain class for this is payment
*/
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainClasses;

namespace LegacyHelperClasses
{

    public class PaymentLegacy
    {
        //This method connects to the existing database using a connection string
        // if the connection is success it update the existing datbase
        public  int ConnectAndUpdate(Payment payment)
        {      
            if (payment == null)
            {
                return 0;
            }
            string strtnotes = "456", strrefno = "123", strbno = "555";
            string conString = "Data Source=regdev;Initial Catalog=Regency Furniture;Integrated Security = True;TrustServerCertificate=True";
            SqlConnection con = new SqlConnection(conString);
            SqlDataAdapter adap = new SqlDataAdapter();
            string sql = "INSERT INTO dbo.Payment (InvNumber,PaymentType,PaymentAmount,PaymentNotes,PaymentDate,PaymentCreateDate,PaymentCreateIP,PaymentCreatedBy,ApprovalCode,BatchNo,RefNo,PaymentLocation) VALUES (";
            // string builder is elegent pattern for query strings.
            StringBuilder sqlbuilder = new StringBuilder();
            sqlbuilder.Append(sql);
            sqlbuilder.AppendFormat("{0},{1},{2},{3},'{4}','{5}','{6}',{7},{8},{9},{10},{11} )", payment.InvoiceNumber, payment.TypeId, payment.Amount, strtnotes, payment.PaymentDate, payment.CreateDate, payment.CreateIP, payment.CreateUserId, strrefno, strbno, strrefno, payment.LocationId);
            try
            {
                // Open connection
                con.Open();
                adap.InsertCommand = new SqlCommand(sqlbuilder.ToString(), con);
                //insert into the database
                adap.InsertCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                string strerr = ex.ToString();
            }

            return 1;

        }

    }

}
